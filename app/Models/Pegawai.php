<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    use HasFactory;

    protected $table = 'pegawai';

    protected $primaryKey = 'pegawai_id';

    // Kalau PK bukan angka dan autoincrement
    // public $incrementing = false;
    // protected $keyType = 'string';

    // Di set jika ingin menggunakan mass asignment
    // protected $fillable = ['nama', 'nip', 'alamat', 'tanggal_lahir', 'keahlian_id', 'status'];

    // Di set jika ada kolom / field yg ingin dihide
    // protected $hidden = ['password'];

    // Di set jika ada kolom yg ingin di proteksi dari mass asignment, jika isi blank maka semua field dapat diassign melalui form submit
    protected $guarded = [];

    public function keahlian()
    {
        return $this->belongsTo(Keahlian::class, 'keahlian_id', 'keahlian_id');
    }
}
