<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Keahlian extends Model
{
    use HasFactory;

    protected $table = 'keahlian';

    protected $primaryKey = 'keahlian_id';

    // Kalau PK bukan angka dan autoincrement
    // public $incrementing = false;
    // protected $keyType = 'string';

    // protected $fillable = ['nama'];

    // protected $hidden = ['password'];

    protected $guarded = [];

    public function pegawai()
    {
        return $this->hasMany(Pegawai::class, 'keahlian_id', 'keahlian_id');
    }
}
