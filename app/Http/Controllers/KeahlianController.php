<?php

namespace App\Http\Controllers;

use App\Models\Keahlian;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KeahlianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list_keahlian = Keahlian::all();
        // $query = Keahlian::select(['keahlian.nama as nama_keahlian', 'pegawai.nama as nama_pegawai'])
        //     ->join('pegawai', 'keahlian.keahlian_id', '=', 'pegawai.keahlian_id')
        //     ->limit(5)
        //     ->get();

        // return response()->json($query);

        return view('keahlian.index', compact('list_keahlian'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('keahlian.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|string'
        ]);

        // dd($request->all());
        // $k = new Keahlian();
        // $k->nama = $request->nama;
        // $k->save();

        Keahlian::create($request->all());

        return redirect()->route('keahlian.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Keahlian  $keahlian
     * @return \Illuminate\Http\Response
     */
    public function show(Keahlian $keahlian)
    {
        dd($keahlian);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Keahlian  $keahlian
     * @return \Illuminate\Http\Response
     */
    public function edit(Keahlian $keahlian)
    {
        return view('keahlian.edit', compact('keahlian'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Keahlian  $keahlian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Keahlian $keahlian)
    {
        $request->validate([
            'nama' => 'required|string'
        ]);

        $keahlian->nama = $request->nama;
        $keahlian->save();

        return redirect()->route('keahlian.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Keahlian  $keahlian
     * @return \Illuminate\Http\Response
     */
    public function destroy(Keahlian $keahlian)
    {
        $keahlian->delete();

        $response['error'] = false;
        $response['message'] = 'Data berhasil dihapus.';
        
        return response()->json($response, 200);
    }
}
