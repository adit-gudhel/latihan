@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Daftar Keahlian') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <a class="btn btn-primary btn-sm mb-3" href="{{ route('keahlian.create') }}">Tambah</a>

                        <table class="table table-striped table-bordered table-hover" id="tbl-form">
                            <thead>
                                <tr>
                                    <th width="10%" class="text-center">ID</th>
                                    <th>Nama Keahlian</th>
                                    <th width="25%" class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($list_keahlian as $row)
                                    <tr>
                                        <td>{{ $row->keahlian_id }}</td>
                                        <td>{{ $row->nama }}</td>
                                        <td>
                                            <a href="{{ route('keahlian.edit', $row->keahlian_id) }}"
                                                class="btn btn-sm btn-warning">Ubah</a> &nbsp;&nbsp;&nbsp;
                                            <a href="javascript:void(0);" data-id="{{ $row->keahlian_id }}"
                                                class="btn btn-sm btn-danger hapus">Hapus</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Nama Keahlian</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        'use strict'

        $(function() {
            $('#tbl-form').delegate('a.hapus', 'click', function(e) {
                let that = $(this),
                    id = that.data('id');

                Swal.fire({
                    title: 'Konfirmasi Hapus?',
                    text: "Ente yaqueen mau hapus data",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Iya dong'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: '{{ url('keahlian') }}/' + id,
                            method: 'POST',
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                            },
                            data: {
                                _method: 'DELETE'
                            },
                            success: function(result) {
                                if (result.error == false) {
                                    Swal.fire(
                                        'Notifikasi',
                                        result.message,
                                        'success'
                                    );

                                    window.location.reload();
                                }
                            }
                        });
                    }
                });
            });
        });
    </script>
@endpush
