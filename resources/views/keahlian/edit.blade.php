@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Ubah Keahlian') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="POST" action="{{ route('keahlian.update', $keahlian->keahlian_id) }}">
                            @csrf
                            @method('PATCH')

                            <div class="form-group row">
                                <label for="nama" class="col-sm-2 col-form-label">Keahlian</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="nama" id="nama"
                                      value="{{ old('nama') ?? $keahlian->nama }}"  placeholder="Nama keahlian">

                                    @error('nama')
                                        {{-- <span class="invalid-feedback" role="alert"> --}}
                                            <strong>{{ $message }}</strong>
                                        {{-- </span> --}}
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-primary mt-3">Perbaharui</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <!-- halo adlin -- !>
@endpush
