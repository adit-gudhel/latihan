@extends('layouts.app')

@section('content')
    <div class="container mb-3">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Advanced Search') }}</div>
                    <div class="card-body">
                        <div class="row">
                            <form method="GET" action="{{ route('pegawai.index') }}">
                                <div class="form-group row mb-3">
                                    <label for="keahlian" class="col-sm-2 col-form-label">Keahlian</label>
                                    <div class="col-sm-10">
                                        <select name="keahlian" id="keahlian" class="form-control">
                                            <option value="">-- Filter Keahlian</option>
                                            @foreach ($keahlians as $r)
                                                <option value="{{ $r->keahlian_id }}">{{ $r->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-3">
                                    <label for="nama" class="col-sm-2 col-form-label">Nama Pegawai</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="nama" id="nama" class="form-control" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="" class="col-sm-2 col-form-label"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-success" id="btn-filter">Cari</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Daftar Keahlian') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <a class="btn btn-primary btn-sm mb-3" href="{{ route('pegawai.create') }}">Tambah</a>

                        <table class="table table-striped table-bordered table-hover" id="tbl-form">
                            <thead>
                                <tr>
                                    <th width="10%" class="text-center">ID</th>
                                    <th>Nama</th>
                                    <th>NIP</th>
                                    <th>Tgl. Lahir</th>
                                    <th>Alamat</th>
                                    <th>Keahlian</th>
                                    <th width="25%" class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pegawais as $row)
                                    <tr>
                                        <td>{{ $row->pegawai_id }}</td>
                                        <td>{{ $row->nm_pegawai }}</td>
                                        <td>{{ $row->nip }}</td>
                                        <td>{{ $row->tanggal_lahir }}</td>
                                        <td>{{ $row->alamat }}</td>
                                        <td>{{ $row->nm_keahlian }}</td>
                                        <td>
                                            <a href="{{ route('pegawai.edit', $row->pegawai_id) }}"
                                                class="btn btn-sm btn-warning">Ubah</a> &nbsp;&nbsp;&nbsp;
                                            <a href="javascript:void(0);" data-id="{{ $row->pegawai_id }}"
                                                class="btn btn-sm btn-danger hapus">Hapus</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Nama</th>
                                    <th>NIP</th>
                                    <th>Tgl. Lahir</th>
                                    <th>Alamat</th>
                                    <th>Keahlian</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        'use strict'

        $(function() {
            $('#tbl-form').delegate('a.hapus', 'click', function(e) {
                let that = $(this),
                    id = that.data('id');

                Swal.fire({
                    title: 'Konfirmasi Hapus?',
                    text: "Ente yaqueen mau hapus data",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Iya dong'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: '{{ url('pegawai') }}/' + id,
                            method: 'POST',
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                            },
                            data: {
                                _method: 'DELETE'
                            },
                            success: function(result) {
                                if (result.error == false) {
                                    Swal.fire(
                                        'Notifikasi',
                                        result.message,
                                        'success'
                                    );

                                    window.location.reload();
                                }
                            }
                        });
                    }
                });
            });
        });
    </script>
@endpush
