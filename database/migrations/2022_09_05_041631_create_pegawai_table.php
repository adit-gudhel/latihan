<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePegawaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pegawai', function (Blueprint $table) {
            // $table->string('kode_sni', 30)->primary();
            $table->smallIncrements('pegawai_id'); // Ekuivalent dengan Tiny Integer (Auto)
            $table->string('nip', 18)->unique();
            $table->string('nama', 50);
            $table->text('alamat')->nullable()->default(NULL);
            $table->date('tanggal_lahir');
            $table->unsignedTinyInteger('keahlian_id')->index();
            $table->timestamps();
        });

        Schema::table('pegawai', function (Blueprint $table) {
            $table->foreign('keahlian_id')
                ->references('keahlian_id')
                ->on('keahlian')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pegawai', function (Blueprint $table) {
            $table->dropForeign('keahlian_id');
        });

        Schema::dropIfExists('pegawai');
    }
}
